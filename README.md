# Listener node for agents aggregator
### How to run?
Install docker swarm at servers

init docker swarm

create mongodb:
`docker service create --name mongo  --publish published=27017,target=27017 mongo`
create listener nodes:
`docker service create --name listener --publish published=8082,target=8082 --replicas 2 pryran/thesis.listener:latest`
