package innometrics.agents.listeneer.jira;

import innometrics.agents.listeneer.jira.model.*;
import innometrics.agents.listeneer.jira.repository.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
public class JiraListenerController {

    public static final String TABLE_NAME = "jira";

    @Autowired
    AddCommentEventRepository addCommentEventRepository;

    @Autowired
    IssueAssignEventRepository issueAssignEventRepository;

    @Autowired
    IssueCreatedEventRepository  issueCreatedEventRepository;

    @Autowired
    IssueUpdateEventRepository issueUpdateEventRepository;

    @Autowired
    LogWorkEventRepository logWorkEventRepository;



    @GetMapping("addComments")
    public void acceptAddCommentEvent(@RequestBody AddCommentEvent event){
        log.info(event);
        addCommentEventRepository.insert(event);
    }

    @GetMapping("issueAssigns")
    public void acceptIssueAssigntEvent(@RequestBody IssueAssignEvent event){
        log.info(event);
        issueAssignEventRepository.insert(event);
    }

    @GetMapping("issueCreates")
    public void acceptIssueCreatesEvent(@RequestBody IssueCreatedEvent event){
        log.info(event);
        issueCreatedEventRepository.insert(event);
    }

    @GetMapping("issueUpdates")
    public void acceptIssueUpdatesEvent(@RequestBody IssueUpdateEvent event){
        log.info(event);
        issueUpdateEventRepository.insert(event);
    }

    @GetMapping("logWorks")
    public void acceptLogWorksEvent(@RequestBody LogWorkEvent event){
        log.info(event);
        logWorkEventRepository.insert(event);
    }


}
