package innometrics.agents.listeneer.jira.model;

import innometrics.agents.listeneer.jira.JiraListenerController;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@Document(collection = JiraListenerController.TABLE_NAME + "_issue_assign_event")
@ToString
public class IssueAssignEvent {

    @Id
    String user;

    String issueId;

    Date created;

    String assignee;



}
