package innometrics.agents.listeneer.jira.repository;

import innometrics.agents.listeneer.jira.model.AddCommentEvent;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddCommentEventRepository extends MongoRepository<AddCommentEvent,String> {
}
