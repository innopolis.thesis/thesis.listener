package innometrics.agents.listeneer.jira.repository;

import innometrics.agents.listeneer.jira.model.AddCommentEvent;
import innometrics.agents.listeneer.jira.model.IssueAssignEvent;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IssueAssignEventRepository extends MongoRepository<IssueAssignEvent,String> {
}
