package innometrics.agents.listeneer.jira.repository;

import innometrics.agents.listeneer.jira.model.AddCommentEvent;
import innometrics.agents.listeneer.jira.model.IssueCreatedEvent;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IssueCreatedEventRepository extends MongoRepository<IssueCreatedEvent,String> {
}
