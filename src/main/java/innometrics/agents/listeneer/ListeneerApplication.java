package innometrics.agents.listeneer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ListeneerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ListeneerApplication.class, args);
	}

}
